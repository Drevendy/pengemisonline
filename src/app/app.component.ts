import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
import { FCM } from '@ionic-native/fcm';
import { ProfilePengemisPage } from "../pages/profile-pengemis/profile-pengemis";
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { NotifPage } from "../pages/notif/notif";
import { PermintaanPage } from "../pages/permintaan/permintaan";
import { PengemisDetailPage } from '../pages/pengemis-detail/pengemis-detail';
import { SelesaipesanPage } from "../pages/selesaipesan/selesaipesan";
import { PesanPage } from "../pages/pesan/pesan";
import {SearchPage} from "../pages/search/search";
import { KonfirmasiPage } from "../pages/konfirmasi/konfirmasi";
import { PengemisOnline }from '../pages/services/PengemisOnline.service';
import { IsiduitPage } from "../pages/isiduit/isiduit";
import { NotifikasiPage } from "../pages/notifikasi/notifikasi";
import { TutorialPage } from "../pages/tutorial/tutorial";
import { DonasiPage } from "../pages/donasi/donasi";
import { LocalNotifications } from '@ionic-native/local-notifications';
import { WithdrawPage } from "../pages/withdraw/withdraw";
import { SelesaiwithdrawPage } from "../pages/selesaiwithdraw/selesaiwithdraw";
import { Device } from '@ionic-native/device';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SigninPage;
//rootPage : any = ProfilePengemisPage;
pages: Array<{title: string, component: any, icon: string}>;
device_uid:any;
  constructor(private fcm: FCM, public platform: Platform,private pengemisonline:PengemisOnline,private localNotifications: LocalNotifications,private device: Device
    // ,public statusBar: StatusBar, public splashScreen: SplashScreen
  ) {
    this.device_uid=this.device.uuid;
    pengemisonline.setDeviceUID(this.device_uid);
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: 'home' },
      { title: 'Notifikasi' ,component:NotifikasiPage, icon: 'notifications'},
      { title: 'Profile', component: ProfilePengemisPage, icon: 'person'  },
     
      { title: 'Pesanan', component: NotifPage, icon: 'cart'  },
      { title: 'Permintaan', component: PermintaanPage, icon: 'briefcase'  },
      //{ title: 'Pesan', component:PesanPage, icon: 'cart' },
      
      { title: 'Isi Duit' ,component:IsiduitPage, icon: 'cash'},
      { title: 'Tarik Saldo' ,component:WithdrawPage, icon: 'card'},    
      
      { title: 'Tutorial' ,component:TutorialPage, icon: 'book'},
      { title: 'Logout' ,component:SigninPage, icon: 'log-out'}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
    //  this.pengemisonline.initializeDatabase().then(()=>{
      //   this.pengemisonline.loginCheck(this.device_uid).then(()=>{
      //     console.log(this.pengemisonline.getLoginState());
      //     if(this.pengemisonline.getLoginState()){
            
      //       this.nav.setRoot(HomePage);
      //     }else{
      //       this.nav.setRoot(SigninPage);
      //     }
      //   });
      // })
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     // this.statusBar.styleDefault();
     // this.splashScreen.hide();
     this.localNotifications.on('click', (notification, state) => {
      console.log(notification['data']);
    
      if(notification['data']["mydata"]=='transaksi'){
        this.nav.setRoot(PermintaanPage);
      }else if(notification['data']['mydata']=='deposit'){
       
          this.nav.setRoot(ProfilePengemisPage);
      
      }else if(notification['data']['mydata']=='donasi'){
        this.pengemisonline.GetUserDataById().subscribe(data=>{
          this.pengemisonline.SetUserData(data);
          this.nav.setRoot(ProfilePengemisPage);
        });
      }

     })
      this.fcm.getToken().then(token=>{
        
        
       
        this.pengemisonline.registerToken(token);
      })

      this.fcm.subscribeToTopic("memek");
      this.fcm.onNotification().subscribe(data=>{
        if(data.wasTapped){
        //alert( JSON.stringify(data) );
          console.log("Received in fore");
          if(data.param=="transaksi"){
            console.log("ada transaksi");
            this.nav.setRoot(PermintaanPage);
          }else if(data.param=='deposit'){
            
          }
        } else {
       // alert( JSON.stringify(data) );
       if(data.param=="transaksi"){
        console.log("ada transaksi");
        this.localNotifications.schedule({
          id: 1,
          title: 'Transaksi!',
          text: 'Kamu mendapatkan pesanan baru',
          data: { mydata: 'transaksi' }
        });
      }else if(data.param=='deposit'){
        this.localNotifications.schedule({
          id: 1,
          title: 'PengemisOnline',
          text: 'Selamat! deposit anda sudah masuk!',
          data: { mydata: 'deposit' }
        });
      }
          console.log("Received in back");
        };
      })
      
      this.fcm.onTokenRefresh().subscribe(token=>{
      // backend.registerToken(token);
          alert( token );
          console.log(token);
      })
    });
  // })
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  LoginCheck(){
    return this.pengemisonline.getLoginState();
  }

  Logout(){
    this.pengemisonline.setLoginState(false);
  }
  
}
