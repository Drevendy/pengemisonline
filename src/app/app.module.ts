import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from "@angular/http";
import { Http,Headers } from "@angular/http";
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import {SearchPage} from "../pages/search/search";
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
import { BadutPage } from "../pages/badut/badut";
import { BersihPage } from "../pages/bersih/bersih";
import { MusisiPage } from "../pages/musisi/musisi";
import { PembantuPage } from "../pages/pembantu/pembantu";
import { TugasPage } from "../pages/tugas/tugas";
import { ProfilePengemisPage } from "../pages/profile-pengemis/profile-pengemis";
import { NotifPage } from "../pages/notif/notif";
import { PesananmasukPage } from "../pages/pesananmasuk/pesananmasuk";
import { PesananpendingPage } from "../pages/pesananpending/pesananpending";
import { PermintaanPage } from "../pages/permintaan/permintaan";
import { PermintaanmasukPage } from "../pages/permintaanmasuk/permintaanmasuk";
import { PermintaanditerimaPage } from "../pages/permintaanditerima/permintaanditerima";
import { PengemisDetailPage } from '../pages/pengemis-detail/pengemis-detail';
import { PesanPage } from "../pages/pesan/pesan";
import { KonfirmasiPage } from "../pages/konfirmasi/konfirmasi";
import { SelesaipesanPage } from "../pages/selesaipesan/selesaipesan";
import { Firebase } from '@ionic-native/firebase';
import { Base64 } from '@ionic-native/base64';
import { FCM } from '@ionic-native/fcm';
import { Geolocation } from '@ionic-native/geolocation';
// import { ProfileModalPage } from "../pages/profile-modal/profile-modal";
import { PengemisOnline } from "../pages/services/PengemisOnline.service";
import { SuperTabsModule } from "ionic2-super-tabs";

import { DonasiPage } from "../pages/donasi/donasi";
import { IsiduitPage } from "../pages/isiduit/isiduit";
import { KonfirmasiisiduitPage } from "../pages/konfirmasiisiduit/konfirmasiisiduit";
import { NotifikasiPage } from "../pages/notifikasi/notifikasi";
import { NotifikasiharianPage } from "../pages/notifikasiharian/notifikasiharian";
import { NotifikasisemuaPage } from "../pages/notifikasisemua/notifikasisemua";
import { TutorialPage } from "../pages/tutorial/tutorial";
import { CarapesanjasaPage } from "../pages/carapesanjasa/carapesanjasa";
import { LocalNotifications } from '@ionic-native/local-notifications';
import { WithdrawPage } from "../pages/withdraw/withdraw";
import { SelesaiwithdrawPage } from "../pages/selesaiwithdraw/selesaiwithdraw";
import { SQLite } from '@ionic-native/sqlite';
import { Device } from '@ionic-native/device';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    BadutPage,
    BersihPage,
    MusisiPage,
    PembantuPage,
    TugasPage,
    ProfilePengemisPage,
    SigninPage,
    SignupPage,
    NotifPage,
    PesananmasukPage,
    PesananpendingPage,
    SearchPage,
    PermintaanditerimaPage,
    PermintaanmasukPage,
    PermintaanPage,
    PengemisDetailPage,
    PesanPage,
    KonfirmasiPage,
    SelesaipesanPage,
    DonasiPage,
    IsiduitPage,
    KonfirmasiisiduitPage,
    NotifikasiPage,
    NotifikasiharianPage,
    NotifikasisemuaPage,
    TutorialPage,
    CarapesanjasaPage,
    WithdrawPage,
    SelesaiwithdrawPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    SuperTabsModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BadutPage,
    BersihPage,
    MusisiPage,
    PembantuPage,
    SearchPage,
    TugasPage,
    ProfilePengemisPage,
    SigninPage,
    SignupPage,
    NotifPage,
    PesananmasukPage,
    PesananpendingPage,
    PermintaanditerimaPage,
    PermintaanmasukPage,
    PermintaanPage,
    PengemisDetailPage,
    PesanPage,
    KonfirmasiPage,
    SelesaipesanPage,
    DonasiPage,
    
    IsiduitPage,
    KonfirmasiisiduitPage,
    NotifikasiPage,
    NotifikasiharianPage,
    NotifikasisemuaPage,
    TutorialPage,
    CarapesanjasaPage,
    WithdrawPage,
    SelesaiwithdrawPage
  ],
  providers: [
    // StatusBar,
    // SplashScreen,
    Crop,
    PengemisOnline,
    ImagePicker,
    Firebase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Base64,
    FCM, 
    Geolocation,
    LocalNotifications,
    SQLite,
    Device,
    NativeGeocoder

  ]
})
export class AppModule {}
