import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { PengemisOnline } from "../services/PengemisOnline.service";
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { PengemisDetailPage } from '../pengemis-detail/pengemis-detail';
import { PesanPage } from "../pesan/pesan";
import {SearchPage} from "../search/search";
/**
 * Generated class for the TugasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  listOfJasaHome:any=[];
  search:string='';
  searchResult:any=[];
  dictionary:any = [];
  keyword:string='';
  constructor(public navCtrl: NavController, public navParams: NavParams,public pengemis_api:PengemisOnline, private domSanitizer:DomSanitizer, private app:App) {
    pengemis_api.GetAllJasa().subscribe(data=>{
      this.dictionary = data;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TugasPage');
    
  }

  SearchResult(){
    this.pengemis_api.ClearSearch();
 
      this.pengemis_api.Search(this.keyword).subscribe(data=>{
  
          this.dictionary = data;
          // /console.log(this.dictionary);
         
          for(let user_data of this.dictionary){
            //this.searchResult.push(user_data);
            this.pengemis_api.AddSearchResult(user_data);
           
          }
       

      });
     this.navCtrl.push(SearchPage);
    }

  ngOnInit(){
    
    //  this.listOfJasaHome= 
    this.pengemis_api.getHomeData().subscribe(data=>{
      this.listOfJasaHome=data;
     // console.log(this.listOfJasaHome);
    })
  }

  parseUrl(url){
  //  url="data:image/png;charset=utf-8;base64, "+url;
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
  CekListCount(){
    let ctr = 0;
    //console.log(this.listOfJasaHome);
    for(let i = 0; i<this.listOfJasaHome.length; i++){
      if(this.listOfJasaHome[i].jasa.length==0) ctr++;
    }
    //console.log(ctr);
    //console.log(this.listOfJasaHome.length);
    if(this.listOfJasaHome.length==ctr) return true;
    else return false;
  }
  SeeDetailPengemis(user){
    console.log(user);
    this.app.getRootNav().push(PengemisDetailPage, {data_user : user});
  }

  PesanJasa(user, jasa){
    console.log(user);
    console.log(jasa);
    console.log("jasa " + jasa['id_jasa']);
   // this.pengemis_api.Testpush(user.pengemis.id).subscribe();
    this.app.getRootNav().push(PesanPage, {data_user : user, data_jasa : jasa});
  }

  potongDeskripsi(deskripsi){

    if (deskripsi.length > 20) {
      deskripsi = deskripsi.substr(0,80);
    }
    return deskripsi;
    
  }
}
