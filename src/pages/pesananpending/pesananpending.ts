import { Component,Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PengemisOnline } from "../services/PengemisOnline.service";

/**
 * Generated class for the PesananpendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pesananpending',
  templateUrl: 'pesananpending.html',
})
export class PesananpendingPage {
  dataPesanan:any=[];
  jasaPesanan:any=[];
  userPengemis:any=[];
 
  constructor(public navCtrl: NavController, public navParams: NavParams,public pengemisapi:PengemisOnline) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesananpendingPage');
  }

  ngOnInit(){
    this.pengemisapi.getPesanan('Pending').subscribe(data=>{
        console.log(data);
        this.dataPesanan=data['data'];
        this.jasaPesanan=data['jasa'];
        this.userPengemis=data['user'];
    })
  }

}
