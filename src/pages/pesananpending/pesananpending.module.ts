import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesananpendingPage } from './pesananpending';

@NgModule({
  declarations: [
    PesananpendingPage,
  ],
  imports: [
    IonicPageModule.forChild(PesananpendingPage),
  ],
})
export class PesananpendingPageModule {}
