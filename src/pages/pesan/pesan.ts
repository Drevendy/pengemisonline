import { Component, ViewChild ,ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { KonfirmasiPage } from "../konfirmasi/konfirmasi";
import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 
import {PengemisOnline}from '../services/PengemisOnline.service';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
/**
 * Generated class for the PesanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()



@Component({
  selector: 'page-pesan',
  templateUrl: 'pesan.html',
})
export class PesanPage {
  options : GeolocationOptions;
  currentPos : Geoposition;
  pesanForm: FormGroup;
  bertemu:any;
  lat:any;
  lng:any;
  validatorObj:any={};
  bertemuField:boolean=true;
  user : any;
  jasa : any;
  self:any;
  disabledbutton:boolean=true;
  static namajalan:any="";
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder : FormBuilder, public alertController:AlertController, private geolocation : Geolocation, private pengemisOnline : PengemisOnline, private app: App,private nativeGeocoder: NativeGeocoder) {
    this.user = this.navParams.get('data_user');
    this.jasa = this.navParams.get('data_jasa');
    console.log(this.user + this.jasa);
    this.self=this.pengemisOnline.GetUserData();
    // this.validatorObj['nama']=[];
    // this.validatorObj['email']=[];
    // this.validatorObj['noHP']=[];
    this.validatorObj['alamat']=[];
    this.validatorObj['tgl_bertemu']=[];
    this.validatorObj['jam_bertemu']=[];
    // this.validatorObj['tempat_bertemu']=[];
    this.validatorObj['bertemu']=[];
    // this.validatorObj['nama'].push('');
    // this.validatorObj['email'].push('');
    // this.validatorObj['noHP'].push('');
    this.validatorObj['alamat'].push('');
    this.validatorObj['tgl_bertemu'].push('');
    this.validatorObj['jam_bertemu'].push('');
    // this.validatorObj['tempat_bertemu'].push('');
    this.validatorObj['bertemu'].push('');
    this.validatorObj['caraberkomunikasi'].push('');
    // this.validatorObj['nama'].push(Validators.required);
    // this.validatorObj['email'].push(Validators.required);
    // this.validatorObj['noHP'].push(Validators.required);
    this.validatorObj['alamat'].push(Validators.required);
    // this.validatorObj['tgl_bertemu'].push(Validators.required);
    // this.validatorObj['jam_bertemu'].push(Validators.required);
     //this.validatorObj['tempat_bertemu'].push(Validators.minLength(0));
    this.validatorObj['bertemu'].push(Validators.required);
    this.pesanForm = this.formBuilder.group(this.validatorObj);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesanPage');
   // this.getUserPosition();
  }

  FormModifier(){
    this.pesanForm = this.formBuilder.group(this.validatorObj);
  }
  addMap(lat,long){
      var geocoder = new google.maps.Geocoder();
      let latLng = new google.maps.LatLng(lat, long);
      // var request = {
      //   latLng: latLng
      // };
      // geocoder.geocode(request, function(data, status) {
      //   if (status == google.maps.GeocoderStatus.OK) {
      //     if (data[0] != null) {
      //       //alert("address is: " + data[0].formatted_address);
      //       console.log("address is: " + data[0].formatted_address);
      //       PesanPage.namajalan=data[0].formatted_address;
      //       console.log(PesanPage.namajalan);
            
      //      // this.SetJalan(this.namajalan);
      //     } else {
      //       alert("No address available");
      //     }
      //   }
      // });
      this.nativeGeocoder.reverseGeocode(lat, long)
      .then((result: NativeGeocoderReverseResult) =>{
        console.log(JSON.stringify(result));
        this.pesanForm.controls['alamat'].setValue(result['subAdministrativeArea']+", "+result['thoroughfare']);
      })
      .catch((error: any) => console.log(error));

      let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
      }
  
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.addMarker();
  
  }
  SetAddress(){
    
    console.log(this.pesanForm.value);
    //this.pesanForm.value["alamat"]=PesanPage.namajalan;
  }
  SetJalan(value){
    this.pesanForm.value['alamat']=value;
  }
  getMap(){
    this.getUserPosition().then(resolve=>{
      console.log(PesanPage.namajalan);
    });

    
  }
  addMarker(){
    
        let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
        });
    
        let content = "<p>This is your current position !</p>";          
        let infoWindow = new google.maps.InfoWindow({
        content: content
        });
    
        google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
        });
    
  }
  onSubmit(){
    
    console.log("this lat = " + this.lat);
    console.log(this.pesanForm.value["bertemu"]);
    if(this.pesanForm.value['bertemu']=="bertemu"){
      if((this.pesanForm.value['tgl_bertemu']==''||this.pesanForm.value['jam_bertemu']=="")){
        this.BuatAlert("Error", "Silahkan lengkapi seluruh data yang dibutuhkan!");
      }
      else{
        this.app.getRootNav().push(KonfirmasiPage, {nama : this.self['name'], nomor : this.self['telefon'], alamat : this.pesanForm.value['alamat'], email : this.self['email'], lat : this.lat, lng : this.lng, id_jasa : this.jasa['id_jasa'], jumlah : this.jasa['harga'], bertemu : 1, tgl_bertemu : this.pesanForm.value['tgl_bertemu'], jam_bertemu : this.pesanForm.value['jam_bertemu'], tempat_bertemu : this.pesanForm.value['alamat'], data_user : this.user, data_jasa : this.jasa});
      }
    }else{
      if(this.pesanForm.value["caraberkomunikasi"]==''){
        this.BuatAlert("Error", "Silahkan lengkapi seluruh data yang dibutuhkan!");
      }else{
        console.log("jago");
        this.app.getRootNav().push(KonfirmasiPage, {nama : this.self['name'], nomor : this.self['telefon'], alamat : this.pesanForm.value['alamat'], email : this.self['email'], lat : this.lat, lng : this.lng, id_jasa : this.jasa['id_jasa'], jumlah : this.jasa['harga'], bertemu : 0, tgl_bertemu : this.pesanForm.value['tgl_bertemu'], jam_bertemu : this.pesanForm.value['jam_bertemu'], tempat_bertemu : this.pesanForm.value['alamat'], data_user : this.user, data_jasa : this.jasa});
      }
      
    }
    //this.navCtrl.setRoot(KonfirmasiPage);
  }

  batal(){
    
  }
  BuatAlert(title, description){
    let alert = this.alertController.create({
      title:title,
      subTitle:description,
      buttons:[
        {
          text:'Ok'
        }
      ]
    })
    alert.present();
  }
  radioCheck(){
    console.log(this.pesanForm.value["bertemu"]);
    if(this.pesanForm.value['bertemu']=="bertemu"){
      this.pesanForm.controls['alamat'].setValue("");
      this.bertemuField=true;
      console.log('bertemu');
     // this.FormModifier();
    }else{
      this.bertemuField=false;
      this.pesanForm.controls['alamat'].setValue("Tidak Ada");
      console.log("tidak");
      //this.FormModifier();
    }
  }
  getUserPosition(){

    return new Promise(resolve => {
      this.options = {
          enableHighAccuracy : true
      };

      this.geolocation.getCurrentPosition(this.options).then((pos : Geoposition) => {

          this.currentPos = pos ;      
          console.log("position = " + pos);
          this.lat = pos.coords.latitude;
          this.lng = pos.coords.longitude;
          this.addMap(pos.coords.latitude, pos.coords.longitude);
          resolve ("memek");
      },(err : PositionError)=>{
            console.log("error : " + err.message);
        });
      });

    
  }

  

}
