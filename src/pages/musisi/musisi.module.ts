import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusisiPage } from './musisi';

@NgModule({
  declarations: [
    MusisiPage,
  ],
  imports: [
    IonicPageModule.forChild(MusisiPage),
  ],
})
export class MusisiPageModule {}
