import { Component ,Input} from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { PesananmasukPage } from "../pesananmasuk/pesananmasuk";
import { PesananpendingPage } from "../pesananpending/pesananpending";
/**
 * Generated class for the NotifPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notif',
  templateUrl: 'notif.html'
})
export class NotifPage {
  

  pesananpendingRoot = PesananpendingPage
  pesananmasukRoot = PesananmasukPage


  constructor(public navCtrl: NavController) {}
  

}
