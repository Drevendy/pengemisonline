import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BersihPage } from './bersih';

@NgModule({
  declarations: [
    BersihPage,
  ],
  imports: [
    IonicPageModule.forChild(BersihPage),
  ],
})
export class BersihPageModule {}
