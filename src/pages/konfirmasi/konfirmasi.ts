import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SelesaipesanPage } from "../selesaipesan/selesaipesan";
import {PengemisOnline}from '../services/PengemisOnline.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
/**
 * Generated class for the KonfirmasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-konfirmasi',
  templateUrl: 'konfirmasi.html',
})
export class KonfirmasiPage {
  nama : any;
  nomor : any;
  alamat : any;
  email : any;
  lat : any;
  lng : any;
  id_jasa : any;
  jumlah : any;
  bertemu : any;
  tgl_bertemu : any;
  jam_bertemu : any;
  tempat_bertemu : any;
  user : any={};
  jasa : any;
  namapengemis:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private pengemisOnline : PengemisOnline, private domSanitizer:DomSanitizer) {
    this.user={};
    this.user = this.navParams.get('data_user');
    this.jasa = this.navParams.get('data_jasa');
    
    console.log(this.user.pengemis);
    this.nama = this.navParams.get('nama');
    this.nomor = this.navParams.get('nomor');
    this.alamat = this.navParams.get('alamat');
    this.email = this.navParams.get('email');
    this.lat = this.navParams.get('lat');
    this.lng = this.navParams.get('lng');
    this.id_jasa = this.navParams.get('id_jasa');
    this.jumlah = this.navParams.get('jumlah');
    this.bertemu = this.navParams.get('bertemu');
    this.tgl_bertemu = this.navParams.get('tgl_bertemu');
    this.jam_bertemu = this.navParams.get('jam_bertemu');
    this.tempat_bertemu = this.navParams.get('tempat_bertemu');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KonfirmasiPage');
    
  }

    klik(){
      this.pengemisOnline.InsertJasa(this.nama, this.nomor, this.alamat, this.email, "berita_transaksi", this.lat, this.lng, this.id_jasa, this.jumlah, this.bertemu, this.tgl_bertemu, this.jam_bertemu, this.tempat_bertemu).subscribe(data=>{
        console.log(data);
        this.pengemisOnline.Testpush("transaksi",this.user['pengemis']['id']).subscribe(data=>{
          this.navCtrl.setRoot(SelesaipesanPage);
        });
        //this.app.getRootNav().push(PengemisDetailPage, {data_user : user});
        
        // if(data[0]['status']){
        //   console.log("memek");
        //   this.pengemisOnline.setLoginState(data[0]['status']);
        //   this.navCtrl.setRoot(HomePage);
        // }else{
        //   this.pengemisOnline.setLoginState(data[0]['status']);
        //   this.loginState=data[0]['status']
        // }
      });
    }

    parseUrl(url){
      //  url="data:image/png;charset=utf-8;base64, "+url;
        return this.domSanitizer.bypassSecurityTrustUrl(url);
      }
}
