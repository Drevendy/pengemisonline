import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KonfirmasiisiduitPage } from './konfirmasiisiduit';

@NgModule({
  declarations: [
    KonfirmasiisiduitPage,
  ],
  imports: [
    IonicPageModule.forChild(KonfirmasiisiduitPage),
  ],
})
export class KonfirmasiisiduitPageModule {}
