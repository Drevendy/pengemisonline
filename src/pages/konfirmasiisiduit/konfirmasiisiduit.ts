import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotifikasiPage } from "../notifikasi/notifikasi";

/**
 * Generated class for the KonfirmasiisiduitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-konfirmasiisiduit',
  templateUrl: 'konfirmasiisiduit.html',
})
export class KonfirmasiisiduitPage {
  jumlah : any;
  bank : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.jumlah = this.navParams.get('jumlah');
    this.bank = this.navParams.get('bank');
    console.log(this.jumlah);
    console.log(this.bank);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KonfirmasiisiduitPage');
  }


  notif(){
    this.navCtrl.setRoot(NotifikasiPage);
  }
}
