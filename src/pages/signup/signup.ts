import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder,FormControl } from '@angular/forms';
import { PengemisOnline }from '../services/PengemisOnline.service';
import { SigninPage } from '../signin/signin';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  kota: string = "Jakarta";
  signupForm : FormGroup;
  data_api: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder : FormBuilder, public pengemisOnline : PengemisOnline) {
    this.signupForm = this.formBuilder.group({
      nama: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,this.emailValidator])],
      password: ['', Validators.required],
      notelp: ['', Validators.required],
      tgl_lahir: ['', Validators.required],
      kota: ['', Validators.required],
      nama_bank: ['', Validators.required],
      norek: ['', Validators.required]
    })
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
      
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }


  emailValidator(control: FormControl): {[key: string]: any}
  {
      var emailRegexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (control.value && !emailRegexp.test(control.value)) return { invalidEmail: true };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
  onSubmit(){
    console.log("signup");

    this.pengemisOnline.register(this.signupForm.value["nama"], this.signupForm.value["email"], this.signupForm.value["password"], this.signupForm.value["notelp"], this.signupForm.value["tgl_lahir"], this.signupForm.value["kota"], this.signupForm.value["nama_bank"], this.signupForm.value["norek"] ).subscribe(data=>{
      this.data_api = data["_body"];
      console.log(data.success);
      if(data.success){
        console.log("berhasil register");
        this.navCtrl.setRoot(SigninPage);
      }else{
        console.log("gagal");
      }
    });
  }
  toSignin(){
    console.log("to signin");
    this.navCtrl.push(SigninPage);
  }

}
