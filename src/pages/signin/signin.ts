import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { PengemisOnline }from '../services/PengemisOnline.service';
import { HomePage } from "../home/home";
import { SignupPage } from '../signup/signup';
/**
 * Generated class for the SigninPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
  signinForm: FormGroup;
  loginState:boolean=true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder : FormBuilder, public pengemisOnline : PengemisOnline) {
    this.signinForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }

  LoginCheck(){
    return this.loginState;
  }
  onSubmit(){
    console.log("signin");
    this.pengemisOnline.login(this.signinForm.value["email"], this.signinForm.value["password"]).subscribe(data=>{
      console.log(data);
      console.log(data[0]['status']);
      if(data[0]['status']){
        console.log("memek");
        this.pengemisOnline.setLoginState(data[0]['status']);
        this.navCtrl.setRoot(HomePage);
      }else{
        this.pengemisOnline.setLoginState(data[0]['status']);
        this.loginState=data[0]['status']
      }
    });
  }
  toSignup(){
    console.log("to signup");
    this.navCtrl.push(SignupPage);
  }
}
