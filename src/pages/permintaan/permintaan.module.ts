import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PermintaanPage } from './permintaan';

@NgModule({
  declarations: [
    PermintaanPage,
  ],
  imports: [
    IonicPageModule.forChild(PermintaanPage),
  ],
})
export class PermintaanPageModule {}
