import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { PermintaanditerimaPage } from "../permintaanditerima/permintaanditerima";
import { PermintaanmasukPage } from "../permintaanmasuk/permintaanmasuk";

/**
 * Generated class for the PermintaanPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-permintaan',
  templateUrl: 'permintaan.html'
})
export class PermintaanPage {

  permintaanmasukRoot = PermintaanmasukPage
  permintaanditerimaRoot = PermintaanditerimaPage


  constructor(public navCtrl: NavController) {}

}
