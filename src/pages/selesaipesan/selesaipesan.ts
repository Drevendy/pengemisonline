import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotifPage } from "../notif/notif";

/**
 * Generated class for the SelesaipesanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selesaipesan',
  templateUrl: 'selesaipesan.html',
})
export class SelesaipesanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelesaipesanPage');
  }

  notif(){
    this.navCtrl.setRoot(NotifPage);
  }

}
