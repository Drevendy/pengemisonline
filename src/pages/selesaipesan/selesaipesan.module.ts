import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelesaipesanPage } from './selesaipesan';

@NgModule({
  declarations: [
    SelesaipesanPage,
  ],
  imports: [
    IonicPageModule.forChild(SelesaipesanPage),
  ],
})
export class SelesaipesanPageModule {}
