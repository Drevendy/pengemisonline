import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IsiduitPage } from './isiduit';

@NgModule({
  declarations: [
    IsiduitPage,
  ],
  imports: [
    IonicPageModule.forChild(IsiduitPage),
  ],
})
export class IsiduitPageModule {}
