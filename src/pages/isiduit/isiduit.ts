import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { AlertController } from "ionic-angular";
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { KonfirmasiisiduitPage } from "../konfirmasiisiduit/konfirmasiisiduit";
import { PengemisOnline }from '../services/PengemisOnline.service';
/**
 * Generated class for the IsiduitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-isiduit',
  templateUrl: 'isiduit.html',
})
export class IsiduitPage {
  loggedInUser : any;
  depositForm : FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertctrl:AlertController, private pengemisOnline : PengemisOnline, private app : App) {
    this.loggedInUser = this.pengemisOnline.GetUserData();
    console.log(this.loggedInUser);
  }
  ngOnInit(){
    this.initializeForm();
  }
  private initializeForm(){
    this.depositForm = new FormGroup({
      jumlah: new FormControl('10000', Validators.required),
      bank: new FormControl('BCA', Validators.required),
      namarekening: new FormControl(null, Validators.required)
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad IsiduitPage');
  }

  Konfirmasi(){
    let alert = this.alertctrl.create({
      title: 'Konfirmasi',
      message: 'Nama Bank<br> '+ this.depositForm.value['bank'] + '<br><br> Jumlah Pengisian<br>Rp.'+this.depositForm.value['jumlah']+',-<br><br>Nama Rekening<br>' + this.depositForm.value['namarekening'],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log("test isi deposit");
            this.pengemisOnline.IsiDeposit(this.loggedInUser.id, this.depositForm.value['jumlah']).subscribe(data=>{
              console.log(data);
              this.pengemisOnline.SetUserData(data);
              this.pengemisOnline.Testpush("deposit").subscribe();
              /*console.log(data[0]['status']);
              if(data[0]['status']){
                console.log("memek");
                this.pengemisOnline.setLoginState(data[0]['status']);
                this.navCtrl.setRoot(HomePage);
              }else{
                this.pengemisOnline.setLoginState(data[0]['status']);
                this.loginState=data[0]['status']
              }*/
            });
            this.app.getRootNav().push(KonfirmasiisiduitPage, {jumlah : this.depositForm.value['jumlah'], bank : this.depositForm.value['bank']});
          }
        }
      ]
    });
    alert.present();
  }

}
