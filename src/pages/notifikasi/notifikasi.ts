import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { NotifikasiharianPage } from "../notifikasiharian/notifikasiharian";
import { NotifikasisemuaPage } from "../notifikasisemua/notifikasisemua";
/**
 * Generated class for the NotifPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifikasi',
  templateUrl: 'notifikasi.html'
})
export class NotifikasiPage {

  notifharianRoot = NotifikasiharianPage;
  notifsemuaRoot = NotifikasisemuaPage;


  constructor(public navCtrl: NavController) {}

}
