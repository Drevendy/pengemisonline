import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
import {PengemisOnline}from '../services/PengemisOnline.service'
/**
 * Generated class for the ProfileModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-modal',
  templateUrl: 'profile-modal.html',
})
export class ProfileModalPage {
  jasaForm : FormGroup;
  jasa:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,
  public pengemisOnline:PengemisOnline) {
    this.jasa = this.navParams.get("Jasa");
    console.log(this.jasa);
    this.InitializeForm();
  }

  
  dismiss(){
   
    this.viewCtrl.dismiss();
  }

  private InitializeForm(){
    this.jasaForm = new FormGroup({
        judulJasa : new FormControl(null, Validators.required),
        nominalPembayaran : new FormControl(null, Validators.required),
        deskripsi : new FormControl(null, Validators.required),
        tag :new FormControl(null, Validators.required)
    })
  }



  private OnSubmit(){

    
  
    //console.log(this.jasaForm.value);
    let tag = "";
    let ListOfTags:any=[];
    let wordCount = this.jasaForm.value.tag.length;
    let userTagInput = this.jasaForm.value.tag;
    let lock = 0;
    //console.log(wordCount);
    for(let i = 0; i < wordCount;i++){
      if((userTagInput[i]==='\n' && tag.length > 0) || i >= wordCount-1){
        lock++;
        ListOfTags.push(tag);
        tag = "";
      }else {
        if(userTagInput[i] != '\n')
            tag+=userTagInput[i];
      }
    }
    console.log(ListOfTags);
    let jasaData={};
    jasaData["judulJasa"]=this.jasaForm.value.judulJasa;
    jasaData["nominalPembayaran"]=parseInt(this.jasaForm.value.nominalPembayaran);
    jasaData["deskripsi"]=this.jasaForm.value.deskripsi;
    jasaData["tag"]=[];
    jasaData["tag"] = ListOfTags;
    console.log(jasaData);
    this.pengemisOnline.SendJasaData(jasaData).subscribe(data=>{
      console.log(data);
      this.dismiss();
    });
  }

}
