import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BadutPage } from './badut';

@NgModule({
  declarations: [
    BadutPage,
  ],
  imports: [
    IonicPageModule.forChild(BadutPage),
  ],
})
export class BadutPageModule {}
