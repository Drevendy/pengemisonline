import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesananmasukPage } from './pesananmasuk';

@NgModule({
  declarations: [
    PesananmasukPage,
  ],
  imports: [
    IonicPageModule.forChild(PesananmasukPage),
  ],
})
export class PesananmasukPageModule {}
