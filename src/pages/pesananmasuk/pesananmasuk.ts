import { Component,Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PengemisOnline } from "../services/PengemisOnline.service";
/**
 * Generated class for the PesananmasukPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pesananmasuk',
  templateUrl: 'pesananmasuk.html',
})
export class PesananmasukPage {
  dataPesanan:any=[];
  jasaPesanan:any=[];
  userPengemis:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public pengemisapi:PengemisOnline) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesananmasukPage');
  }
  ngOnInit(){
    this.pengemisapi.getPesanan('Accepted').subscribe(data=>{
        console.log(data);
        this.dataPesanan=data['data'];
        this.jasaPesanan=data['jasa'];
        this.userPengemis=data['user'];
    })
  }
  Done(id_transaksi){
    console.log(id_transaksi);
    this.pengemisapi.SetRequesterTrue(id_transaksi).subscribe(data=>{
      console.log(data);
      if(data.receiver_accept == "1" && data.requester_accept=="1"){
        this.pengemisapi.SetDone( id_transaksi).subscribe();
      } 
    });
  }
}
