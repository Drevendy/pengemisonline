import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { PengemisOnline } from "../services/PengemisOnline.service";
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { PengemisDetailPage } from '../pengemis-detail/pengemis-detail';
import { PesanPage } from "../pesan/pesan";

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  listOfJasaHome:any=[];
  //listOfMemek:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public pengemis_api:PengemisOnline, private domSanitizer:DomSanitizer, private app:App) {
  //this.listOfMemek = navParams.get("searchResult");
     // console.log(this.listOfJasaHome);
  }

  ionViewDidLoad() {
   // console.log('ionViewDidLoad TugasPage');
    
  }

ngOnInit(){
    
    //  this.listOfJasaHome= 
    //this.pengemis_api.getHomeData().subscribe(data=>{
     // this.listOfJasaHome=data;
     // console.log(this.listOfJasaHome);
    //})
    this.listOfJasaHome = this.pengemis_api.GetSearchResult();
  }


  parseUrl(url){
  //  url="data:image/png;charset=utf-8;base64, "+url;
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
  CekListCount(){
    let ctr = 0;
    //console.log(this.listOfJasaHome);
    for(let i = 0; i<this.listOfJasaHome.length; i++){
      if(this.listOfJasaHome[i].jasa.length==0) ctr++;
    }
    //console.log(ctr);
    //console.log(this.listOfJasaHome.length);
    if(this.listOfJasaHome.length==ctr) return true;
    else return false;
  }
  SeeDetailPengemis(user){
    console.log(user);
    this.app.getRootNav().push(PengemisDetailPage, {data_user : user});
  }

  PesanJasa(user){
    //console.log(user);
    this.pengemis_api.Testpush(user.pengemis.id).subscribe();
    this.app.getRootNav().push(PesanPage, {data_user : user});
  }

  potongDeskripsi(deskripsi){

    if (deskripsi.length > 20) {
      deskripsi = deskripsi.substr(0,80);
    }
    return deskripsi;
    
  }
}
