import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PermintaanmasukPage } from './permintaanmasuk';

@NgModule({
  declarations: [
    PermintaanmasukPage,
  ],
  imports: [
    IonicPageModule.forChild(PermintaanmasukPage),
  ],
})
export class PermintaanmasukPageModule {}
