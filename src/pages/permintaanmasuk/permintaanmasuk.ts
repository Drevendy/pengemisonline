import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController, Platform, ViewController,AlertController } from 'ionic-angular';
import {PengemisOnline} from '../services/PengemisOnline.service';
/**
 * Generated class for the PermintaanmasukPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-permintaanmasuk',
  templateUrl: 'permintaanmasuk.html',
})
export class PermintaanmasukPage {
  private dataPermintaan:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private pengemis_api:PengemisOnline,
    public alertController:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PermintaanmasukPage');
  }
  ngOnInit(){
    this.dataPermintaan=[];
    this.pengemis_api.getPermintaan("Pending").subscribe(data=>{
      //console.log(data['data']);
      this.dataPermintaan = data['data'];
      console.log(this.dataPermintaan);
    });
  }
  Refresh(){
    this.dataPermintaan = [];
    this.pengemis_api.getPermintaan("Pending").subscribe(data=>{
      //console.log(data['data']);
      this.dataPermintaan = data['data'];
     // console.log(this.dataPermintaan);
    });
  }
  jasaAccepted(){
    this.pengemis_api.AcceptTransaction( this.dataPermintaan[0]['transaksi']['id']).subscribe();
    this.Refresh();
  }
  deletePermintaan(){
    let alert = this.alertController.create({
      title:"Cancel",
      subTitle:"Apakan anda ingin membatalkan ?",
      buttons:[
        {
          text:'Cancel',
          role:'cancel'
          }
        ,
        {
          text:'Ok',
          handler:()=>{
              this.pengemis_api.CancelTransaction(this.dataPermintaan[0]['transaksi']['id']).subscribe();
              this.Refresh();
            }
          }
      ]
    })
    alert.present();
  }
}
