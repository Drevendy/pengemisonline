import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from "ionic-angular";
import { HomePage } from "../home/home";
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { PengemisOnline }from '../services/PengemisOnline.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';

/**
 * Generated class for the DonasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-donasi',
  templateUrl: 'donasi.html',
})
export class DonasiPage {
  user : any;
  loggedInUser : any;
  donasiForm : FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl:AlertController, private pengemisOnline : PengemisOnline,private domSanitizer:DomSanitizer) {
    this.user = this.navParams.get('data_user');
    this.loggedInUser = this.pengemisOnline.GetUserData();
    console.log(this.user);
  }
  ngOnInit(){
    this.initializeForm();
  }
  private initializeForm(){
    this.donasiForm = new FormGroup({
      donasi: new FormControl('', Validators.required),
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DonasiPage');
  }

  parseUrl(url){
    //  url="data:image/png;charset=utf-8;base64, "+url;
      return this.domSanitizer.bypassSecurityTrustUrl(url);
    }

  donasialert(){
    if(this.loggedInUser.saldo<this.donasiForm.value['donasi']){
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'Saldo anda tidak cukup!',
        buttons: [
          {
            text: 'OK',
            handler: () => {
            }
          }
        ]
      });
      alert.present();
    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Donasi',
        message: 'Apakah anda yakin ingin memberikan donasi?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'OK',
            handler: () => {
              console.log(this.loggedInUser.id);
              console.log(this.user.pengemis.id);
              console.log(this.donasiForm.value['donasi']);
              this.pengemisOnline.Donasi(this.loggedInUser.id, this.user.pengemis.id, this.donasiForm.value['donasi']).subscribe(data=>{
                console.log(data);
                this.pengemisOnline.SetUserData(data);
                this.pengemisOnline.Testpush('donasi',this.user.pengemis.id).subscribe();
                /*console.log(data[0]['status']);
                if(data[0]['status']){
                  console.log("memek");
                  this.pengemisOnline.setLoginState(data[0]['status']);
                  this.navCtrl.setRoot(HomePage);
                }else{
                  this.pengemisOnline.setLoginState(data[0]['status']);
                  this.loginState=data[0]['status']
                }*/
              });
              this.navCtrl.setRoot(HomePage);
            }
          }
        ]
      });
      alert.present();
    }
    
  }

}
