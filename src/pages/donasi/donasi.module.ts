import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DonasiPage } from './donasi';

@NgModule({
  declarations: [
    DonasiPage,
  ],
  imports: [
    IonicPageModule.forChild(DonasiPage),
  ],
})
export class DonasiPageModule {}
