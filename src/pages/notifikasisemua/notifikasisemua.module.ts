import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotifikasisemuaPage } from './notifikasisemua';

@NgModule({
  declarations: [
    NotifikasisemuaPage,
  ],
  imports: [
    IonicPageModule.forChild(NotifikasisemuaPage),
  ],
})
export class NotifikasisemuaPageModule {}
