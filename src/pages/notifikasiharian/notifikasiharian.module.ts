import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotifikasiharianPage } from './notifikasiharian';

@NgModule({
  declarations: [
    NotifikasiharianPage,
  ],
  imports: [
    IonicPageModule.forChild(NotifikasiharianPage),
  ],
})
export class NotifikasiharianPageModule {}
