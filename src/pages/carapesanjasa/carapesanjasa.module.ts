import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarapesanjasaPage } from './carapesanjasa';

@NgModule({
  declarations: [
    CarapesanjasaPage,
  ],
  imports: [
    IonicPageModule.forChild(CarapesanjasaPage),
  ],
})
export class CarapesanjasaPageModule {}
