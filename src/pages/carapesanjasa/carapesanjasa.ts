import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TutorialPage } from "../tutorial/tutorial";
/**
 * Generated class for the CarapesanjasaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-carapesanjasa',
  templateUrl: 'carapesanjasa.html',
})
export class CarapesanjasaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CarapesanjasaPage');
  }
  kembali(){
    this.navCtrl.setRoot(TutorialPage);
  }

}
