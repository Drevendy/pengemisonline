import { Injectable }                   from '@angular/core';
import { Http, Headers, Response, RequestOptions }      from '@angular/http';
import { Observable }                   from 'rxjs/Observable';
import 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { FCM } from '@ionic-native/fcm';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AlertController,Events } from 'ionic-angular';

@Injectable()
export class PengemisOnline{
  //  public static API_PENGEMIS = "http://localhost/pengemisonlineapi/public/";
    public static API_PENGEMIS = "http://honeymoonlagi.com/laravel/pengemisonlineapi/public/";
    //public static API_PENGEMIS="http://192.168.43.155/pengemisonlineapi/public/"
    public static id_user="";
    public static username="";
    public static email="";
    public static telefon="";
    private static loginState=false;
    private static token="";
    private static alamt = "";
    private static UserData={};
    private static SearchResult:any=[];
    public static system_data : SQLiteObject;
    public static login_checked:boolean=false;
    public static device_id:any="";
    
    constructor(private http : Http,private fcm:FCM,private sqlite: SQLite,private alertCtrl : AlertController)
    {
        
    }
    //SEARCH
        AddSearchResult(data){
            PengemisOnline.SearchResult.push(data);
        }
        ClearSearch(){
            PengemisOnline.SearchResult = [];
        }
        GetSearchResult(){
            console.log(PengemisOnline.SearchResult);   
            return PengemisOnline.SearchResult;
        }
    //SEARACH

    //FCM TOKEN
    registerToken(token){
        PengemisOnline.token=token;
    }
    SetAlamat(alamat){
        PengemisOnline.alamt=alamat;
    }
    GetAlamat(){
        return PengemisOnline.alamt;
    }
    Testpush(jenis,ids=PengemisOnline.UserData[0]['data']['id']):Observable<any>{

        let body = {
            id:ids,
            jenis:jenis
        };

        console.log(body);
         let headers = new Headers();         
         headers.append('Content-Type','application/json');  
         //console.log(JSON.stringify(body));
      
          //   console.log(NewJasa);
             return this .http 
             .post(PengemisOnline.API_PENGEMIS+"transaksi/sendpush",body,{headers:headers}) 
             .map((data) => { 
                return data;
             }) 
    }
//SEARCH
Search(keyword){
    let headers = new Headers(); 
    let body = {
      search_key:keyword
    }
    headers.append('Content-Type','application/json');  
    console.log(JSON.stringify(body));
    return this .http 
        .post(PengemisOnline.API_PENGEMIS+"other/search2", body,{headers:headers}) 
        .map((data) => { 
           return data.json();
        }) 
}

//SERACH


  //JASA
    GetAllJasa():Observable<any>{
        return this .http 
            .get(PengemisOnline.API_PENGEMIS+"jasa/getjasa") 
            .map((response: Response) =>
            {
                let temp    = response.json();
                let retval  = [];
                retval=response.json();
                return retval;
            });
    }

    GetSpecificJasa(_id_jasa):Observable<any>{
        let headers = new Headers(); 
        let body = {
            id_jasa:_id_jasa
        }
        return this .http
        .post(PengemisOnline.API_PENGEMIS+"jasa/getspesifikjasa", body,{headers:headers}) 
        .map((data) => { 
           return data.json();
        }) 

    }

    GetAllJasaData():Observable<any>{
        
        let headers = new Headers(); 
        let body = {
            id_user:PengemisOnline.UserData[0]['data']['id']
        }
        headers.append('Content-Type','application/json');  
        console.log(JSON.stringify(body));
        return this .http 
            .post(PengemisOnline.API_PENGEMIS+"jasa/getuserjasa", body,{headers:headers}) 
            .map((data) => { 
               return data.json();
            }) 

    }

    DeleteJasa(id):Observable<any>{
       let body = {
           id:id
       }
       console.log(body);
        let headers = new Headers();         
        headers.append('Content-Type','application/json');  
        //console.log(JSON.stringify(body));
     
         //   console.log(NewJasa);
            return this .http 
            .post(PengemisOnline.API_PENGEMIS+"jasa/deletejasa",body,{headers:headers}) 
            .map((data) => { 
               return data;
            }) 
        
      
    }


    //USER

    getHomeData(): Observable<any>
    {
        return this .http
                    .get(PengemisOnline.API_PENGEMIS+"jasa/getjasa")
                    .map((response: Response) =>
                    {
                        let temp    = response.json();
                        let retval  = [];
                        let ctr     = 0;
                        retval=response.json();
                        // while (temp["sicepat"][ctr] != null)
                        // {
                        //     retval[ctr]         = [];
                        //     retval[ctr]["name"] = temp["rajaongkir"]["results"][ctr]["province"];
                        //     ctr++;
                        // }

                        return retval;
                            
                    });
    }

    UpdateUser(NewUserData):Observable<any>{
        console.log(NewUserData);
        let headers = new Headers();         
        headers.append('Content-Type','application/json');  
        //console.log(JSON.stringify(body));
        return this .http 
            .post(PengemisOnline.API_PENGEMIS+"user/updateuser   ",NewUserData,{headers:headers}) 
            .map((data) => { 
               return data;
            }) 
    }

    SendJasaData(jasaData):Observable<any>{
       // console.log("KONTOL ANJING");
      
        let headers = new Headers(); 
        let tags: any[];
        let body = {
            id_user:PengemisOnline.UserData[0]['user_id'],
            deskripsi:jasaData["deskripsi"],
            nama:jasaData["judulJasa"],
            harga:jasaData["nominalPembayaran"],
            tags:jasaData["tag"]
        }
        
        headers.append('Content-Type','application/json');  
        console.log(body);
        return this .http 
            .post(PengemisOnline.API_PENGEMIS+"jasa/memek   ",body,{headers:headers}) 
            .map((data) => { 
            
               return data;
    
            }) 


    }

    login(email, password):Observable<any>{
        let headers = new Headers(); 
        let body = {
            email:email,
            password:password,
            token:PengemisOnline.token,
            device_id:PengemisOnline.device_id
        }
        headers.append('Content-Type','application/json');  
        console.log(JSON.stringify(body));
        return this .http 
            .post(PengemisOnline.API_PENGEMIS+"user/logindevice", JSON.stringify(body),{headers:headers}) 
            .map((data) => { 
                //console.log(data.json());
                let retval=data.json();
                console.log(retval);
                PengemisOnline.UserData = retval;
                // PengemisOnline.system_data.executeSql("DELETE FROM logindata", {})
                // PengemisOnline.system_data.executeSql("INSERT INTO logindata(id_login,email,password,device_uid,device_type) VALUES (" + PengemisOnline.UserData[0]['user_id'] + "',  '" +  email + "', '" + password + "', '" +  PengemisOnline.UserData[0]['data']['device_id'] + "', 'android');", {}).then(data1=>{
                //     console.log(data1);
                // });
                
                //this.setUserId(retval[0]['data']['id']);
                //this.setUserEmail(retval[0]['data']['email']);
                //this.setTelefon(retval[0]['data']['telefon']);
                //this.setUserName(retval[0]['data']['name']);
                return data.json();
                
            }) 


    }



    register(nama, email, password, notelp, tgl_lahir, kota, jenis_bank, nomor_rekening):Observable<any>{
        let headers = new Headers(); 
        let body = {
            name: nama,
            email:email,
            password:password,
            telefon: notelp,
            tgl_lahir : tgl_lahir,
            kota : kota,
            jenis_bank : jenis_bank,
            nomor_rekening : nomor_rekening,
            token:PengemisOnline.token
        }
        headers.append('Content-Type','application/json');  
console.log(JSON.stringify(body));
        return this .http 
            .post(PengemisOnline.API_PENGEMIS+"user/register", JSON.stringify(body),{headers:headers}) 
            .map((data) => { 
                return data.json();
            }) 
    }

    getPesanan(status){
        let headers = new Headers(); 
        let body={
            email:PengemisOnline.UserData[0]['data']['email'],
            status:status
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"transaksi/getpesanan", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            return data.json();
        }) 
    }
    getPermintaan(status){
        let headers = new Headers(); 
        let body={
            id:PengemisOnline.UserData[0]['data']['id'],
            status:status
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"transaksi/getpermintaan", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            return data.json();
        }) 
    }

    GetUserDataById(_id=PengemisOnline.UserData[0]['user_id']):Observable<any>{
        let headers = new Headers(); 
        let body={
            id:_id
        };  
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"user/getuserbyid", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            return data.json();
        }) 
    }

    InsertJasa(nama, telepon, alamat, email, berita_transaksi, lat, lng, id_jasa, jumlah_konfirmasi, bertemu, tanggal_bertemu, jam_bertemu, tempat_bertemu){
        let headers = new Headers(); 
        let body={
            nama:nama,
            telefon:telepon,
            alamat:alamat,
            email:email,
            lat:lat,
            lng:lng,
            jumlah_transaksi:jumlah_konfirmasi,
            berita_transaksi:berita_transaksi,
            id_jasa:id_jasa,
            bertemu:bertemu,
            tanggal_bertemu:tanggal_bertemu,
            jam_bertemu:jam_bertemu,
            tempat_bertemu:tempat_bertemu
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"transaksi/insert", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            return data.json();
        }) 
    }

    
    IsiDeposit(id, jumlah){
        let headers = new Headers(); 
        let body={
            id:id,
            jumlah:jumlah,
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"user/isideposit", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            console.log(data);
            return data.json();
        }) 
    }
    Donasi(id_donatur, id_penerima, jumlah_donasi){
        let headers = new Headers(); 
        let body={
            id_donatur:id_donatur,
            id_penerima:id_penerima,
            jumlah: jumlah_donasi
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"user/donasi", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            console.log(data);
            return data.json();
        }) 
    }
    Withdraw(id, jumlah){
        let headers = new Headers(); 
        let body={
            id:id,
            jumlah:jumlah,
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"user/withdraw", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            console.log(data);
            return data.json();
        }) 
    }

    CancelTransaction(transaction_id){
        let headers = new Headers(); 
        let body={
            id:transaction_id
               };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http
        .post(PengemisOnline.API_PENGEMIS+"transaksi/deletetransaction", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            console.log(data);
            return data.json();
        }) 
    }

SetRequesterTrue(id_transaksi):Observable<any>{
    let headers = new Headers(); 
    let body={
        id:id_transaksi
    };
    console.log(body);
    headers.append('Content-Type','application/json');  
    return this .http 
    .post(PengemisOnline.API_PENGEMIS+"transaksi/approve_request", body,{headers:headers}) 
    .map((data) => { 
        console.log(data);
        return data.json();
    }) 
}

    AcceptTransaction(transaction_id){
        let headers = new Headers(); 
        let body={
            id:transaction_id
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"transaksi/setstatusaccepted", JSON.stringify(body),{headers:headers})
        .map((data) => { 
            console.log(data);
            return data.json();
        }) 
    }
//    / SetDone(transaction_id){
//         let headers = new Headers(); 
//         let body={
//             id:transaction_id
//         };
//         console.log(body);
//         headers.append('Content-Type','application/json');  
//         return this .http 
//         .post(PengemisOnline.API_PENGEMIS+"transaksi/setstatusdone", JSON.stringify(body),{headers:headers}) 
//         .map((data) => { 
//             console.log(data);
//             return data.json();
//         }) 
//     }
    // Done(id, jumlah){
    //     let headers = new Headers(); 
    //     let body={
    //         id:id,
    //         jumlah:jumlah,
    //     };
    //     console.log(body);
    //     headers.append('Content-Type','application/json');  
    //     return this .http 
    //     .post(PengemisOnline.API_PENGEMIS+"user/withdraw", JSON.stringify(body),{headers:headers}) 
    //     .map((data) => { 
    //         console.log(data);
    //         return data.json();
    //     }) 
    // }
    // getCityList(province,city): Observable<any>
    // {   
    //     province=province.replace(' ','%20');
    //     let headers = new Headers(); 
    //     let body = 
    //     {  
    //         city : city
    //     };
    //     // console.log('sicepat city'); 
    //     // console.log(body); 
        
    //         headers.append('Content-Type','application/json');  

    //             return this .http 
    //                 .post(SiCepat.API_URL_CITY+province, JSON.stringify(body),{headers:headers}) 
    //                 .map((data) => { 
    //                     console.log(data.json());
    //                     if(data.json().success){ 
    //                         if(city==''){
    //                             return data.json().sicepat_city;
    //                         }else{
    //                             return data.json().sicepat;
    //                         }    
    //                     }else{ 
    //                             return false;
    //                     } 
    
    //                 }) 
    // }

    // getTariff(origin, destination, weight): Observable<any>
    // {
    //     if(weight<1){
    //         weight=1;
    //     }
    //     let headers = new Headers(); 
    //     let body = 
    //     {  
    //         origin      : origin,
    //         destination : destination,
    //         weight      : weight
    //     };
    //     // console.log('sicepat city'); 
    //     // console.log(body); 
        
    //         headers.append('Content-Type','application/json');  

    //             return this .http 
    //                 .post(SiCepat.API_URL_COST, JSON.stringify(body),{headers:headers}) 
    //                 .map((data) => { 
    //                     console.log(data.json());
    //                     if(data.json().success){ 
    //                         return data.json().sicepat;
    //                     }else{ 
    //                         return false
    //                     } 
    
    //                 }) 

    // }

    initializeDatabase(): Promise<any>
    {
        return this.sqlite.create(
        {
            name: 'pengemisonline.db',
            location: 'default'
        })
        .then((db: SQLiteObject) =>
        {
            console.log("----- Database Initialization -----");
            console.log("[MASTER DATA] Database successfully initialized");
            PengemisOnline.system_data = db;
            // Initialize table order

            // Initialize table login data
            PengemisOnline.system_data.executeSql("CREATE TABLE logindata(id_login varchar(20) ,email varchar(50),password varchar(50),device_uid varchar(30),device_type varchar(20))", {}).then(()=>
            { console.log("logindata initialized"); },
            error => { console.log(" logindata already exist"); });;
            
        },
        error =>
        {
            console.log(" Cannot create database, possible native not supported");
            console.log(" Reattempting to connect to database...");
        });
        
    }

    loginCheck(device_uid):Promise<any>
    { 
        var tempCarts = {}; 
        console.log("[MASTER DATA] ----- Login function called -----"); 
    
        return PengemisOnline.system_data.executeSql("SELECT * FROM logindata", {}).then((data)=>
        { 
            console.log("[MASTER DATA] Query successful"); 
            if(data.rows.length>0)
            { 
                console.log("[MASTER DATA] Content exist"); 
                tempCarts['id_login']=data.rows.item(0)['id_login']; 
                tempCarts['password']=data.rows.item(0)['password']; 
                tempCarts['email']=data.rows.item(0)['email']; 

                return this.checkLogindb(data.rows.item(0)['id_login'], device_uid).subscribe(data=>
                { 
                        console.log(data);
                        if(data.success)
                            {
                            if(data.device_id==device_uid){
                                //berhasil
                            }  
                            else
                            {
                                PengemisOnline.loginState=false;
                                let alert = this.alertCtrl.create(
                                {
                                    title: 'Login Error',
                                    message: 'Other device just logged in with your credentials. This account will be logged out now.',
                                    buttons:
                                    [
                                        {
                                            text: 'Okay',
                                            role: 'cancel',
                                            handler: () =>
                                            {
                                                this.logout();
                                            }
                                        }
                                    ]
                                });
                                alert.present();
                            } 
                        }
                        else PengemisOnline.loginState= false; 
                    
                    if (PengemisOnline.loginState == false) console.log("[MASTER DATA] Login failed"); 
                    else console.log("[MASTER DATA] Login success"); 

                    console.log("[MASTER DATA] ---------------------------------"); 

                    return data;
                });  
            }
            else
            {
                console.log("[MASTER DATA] Login failed"); 
                console.log("[MASTER DATA] ---------------------------------");  
                PengemisOnline.loginState= false; 
            } 
        }); 
    } 

    checkLogindb(id,device_id):Observable<any>{
        let headers = new Headers(); 
        let body={
            id:id,
            device_id:device_id,
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"user/checkdevice", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            console.log(data);
            return data.json();
        }) 
    }

    logout(): Promise<any>
    {
        return PengemisOnline.system_data.executeSql("DELETE FROM logindata", {}).then(()=>{
            PengemisOnline.loginState=false;
        });
       
       
    }

    setUserId($id){PengemisOnline.id_user=$id};
    setUserEmail($email){PengemisOnline.email=$email};
    setUserName($username){PengemisOnline.username=$username};
    setTelefon($telefon){PengemisOnline.telefon=$telefon};
    getUserId(){return PengemisOnline.id_user};
    getUserEmail(){return PengemisOnline.email};
    getUserName(){return PengemisOnline.username};
    getTelefon(){return PengemisOnline.telefon};
    setLoginState(state){PengemisOnline.loginState=state};
    getLoginState(){return PengemisOnline.loginState};
    GetUserData(){
       // console.log(PengemisOnline.UserData[0]['data'])
        return PengemisOnline.UserData[0]['data'];
    }
    SetUserData(user){
        PengemisOnline.UserData[0]['data']= user;
    }
    setDeviceUID(id){
        PengemisOnline.device_id=id;
    }














    SetDone(transaction_id){
        let headers = new Headers(); 
        let body={
            id:transaction_id
        };
        console.log(body);
        headers.append('Content-Type','application/json');  
        return this .http 
        .post(PengemisOnline.API_PENGEMIS+"transaksi/setstatusdone", JSON.stringify(body),{headers:headers}) 
        .map((data) => { 
            console.log(data);
            return data.json();
        }) 
    }

    getUser(){
        return PengemisOnline.UserData[0]['data'];
    }

}