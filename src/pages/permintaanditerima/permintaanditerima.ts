import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PengemisOnline} from '../services/PengemisOnline.service';
/**
 * Generated class for the PermintaanditerimaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-permintaanditerima',
  templateUrl: 'permintaanditerima.html',
})
export class PermintaanditerimaPage {
  private dataPermintaan:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private pengemis_api:PengemisOnline) {
  }
  ngOnInit(){
    this.dataPermintaan=[];
    this.pengemis_api.getPermintaan("Accepted").subscribe(data=>{
      //console.log(data['data']);
      this.dataPermintaan = data['data'];
      console.log(this.dataPermintaan);
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PermintaanditerimaPage');
  }

  Done(id_transaksi){
    console.log(id_transaksi);
    this.pengemis_api.SetRequesterTrue(id_transaksi).subscribe(data=>{
      console.log(data);
      if(data.receiver_accept == "1" && data.requester_accept=="1"){
        this.pengemis_api.SetDone( id_transaksi).subscribe();
      } 
    });
  }

}
