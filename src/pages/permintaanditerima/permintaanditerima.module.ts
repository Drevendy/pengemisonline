import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PermintaanditerimaPage } from './permintaanditerima';

@NgModule({
  declarations: [
    PermintaanditerimaPage,
  ],
  imports: [
    IonicPageModule.forChild(PermintaanditerimaPage),
  ],
})
export class PermintaanditerimaPageModule {}
