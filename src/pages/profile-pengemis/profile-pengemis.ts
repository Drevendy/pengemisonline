import { Component } from '@angular/core';
import {Crop}from '@ionic-native/crop'
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController, Platform, ViewController,AlertController } from 'ionic-angular';
import {PengemisOnline} from '../services/PengemisOnline.service';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
/**
 * Generated class for the ProfilePengemisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-pengemis',
  templateUrl: 'profile-pengemis.html',
})
export class ProfilePengemisPage {
  ListOfJasa:any = [];
  UserData:any;
  UserDataRollback:any;
  constructor (public modalCtrl : ModalController ,public navCtrl: NavController, public navParams: NavParams,
      public pengemisOnline:PengemisOnline, public alertController:AlertController, private domSanitizer:DomSanitizer,
      private imagePicker: ImagePicker, private base64: Base64,private crop:Crop) {
  
  }
  
  openModal(){
    let jada = {
      nama:"",
      deskripsi:"",
      harga:"",
      tag:""
    }
    let modal = this.modalCtrl.create("ProfileModalPage",{Jasa:jada});
    modal.onDidDismiss(() => {
      this.SeedData();
    });
    modal.present();
  }

  OpenModalWithData(_id){
    let jasa:any;
    this.pengemisOnline.GetSpecificJasa(_id).subscribe(data=>{
       jasa = data;
       //console.log(jasa);
       let modal = this.modalCtrl.create("ProfileModalPage",{Jasa:jasa});
       modal.onDidDismiss(() => {
         this.RefreshJasa();
       });
       modal.present();
    });
    
  }

  ionViewDidLoad() { 
    console.log('ionViewDidLoad ProfilePengemisPage');
  }
  ngOnInit(){
    this.SeedData();
  
  }

  SeedData(){
   // this.PickImage();
    //this.ListOfJasa = {};
    this.pengemisOnline.GetAllJasaData().subscribe(data=>{
     this.ListOfJasa = data["data"]; 
     // console.log(this.ListOfJasa);
    });
   // this.UserData = {};
    this.UserData = this.pengemisOnline.GetUserData();
    this.UserDataRollback=this.UserData['deskripsi'];
  //  console.log(this.UserData);
  }

  RefreshJasa(){
    this.pengemisOnline.GetAllJasaData().subscribe(data=>{
      this.ListOfJasa = data["data"]; 
     });
  }

  ChangeProfilePicture(){
    this.PickImage();
    console.log("change profile picture");
  }


  PickImage(){
   
    console.log("pickimage");
    let options = {
      maximumImagesCount: 1,
      width: 150,
      heigth: 150,
      quality: 100
    }
    this.imagePicker.getPictures(options).then((results) => {
    //  let filePath: string = results;
     // console.log(results);
     // console.log(results[0]);
    
        this.base64.encodeFile(results[0]).then((base64File: string) => {
          //console.log(base64File);
          this.UserData['profile_picture'] = base64File;
          this.pengemisOnline.UpdateUser(this.UserData).subscribe();
          this.SeedData();
        }, (err) => {
          console.log(err);
        });
      }, (err) => { });
    
     
  }
  

  UpdateDescription(){
    //request database 
  }

  CreateAlert(type,_id){
    /*
     type : 
      0 = update description
      1 = delete jasa 
      2 = update jasa 
    */

    let tittle ="";
    let description = "";
    //this.UserDataRollback = this.pengemisOnline.GetUserData();
   // console.log(this.UserDataRollback);
   // console.log(userDescriptionTemp);
    switch(type){
      case 0: 
          tittle = "Save changes ?"
          break;
      case 1:
          tittle = "Delete"
          description = "Are you sure you want to DELETE this jasa?"
          break;
      case 2:
          tittle = "Edit"
          description = "Are you sure you want to EDIT this jasa?"
          break;
    }

    let alert = this.alertController.create({
      title:tittle,
      subTitle:description,
      buttons:[
        {
          text:'Cancel',
          role:'cancel',
          handler:()=>{
            switch(type){
              case 0 :
                this.UserData['deskripsi'] = this.UserDataRollback;
                console.log(this.UserDataRollback);
                break;
            }
          }
        },
        {
          text:'Ok',
          handler:()=>{
            switch(type){
              case 0:  
                //console.log(this.UserData["deskripsi"]);
                this.pengemisOnline.UpdateUser(this.UserData).subscribe();break;
              case 1:  
                this.pengemisOnline.DeleteJasa( _id).subscribe();
                this.RefreshJasa();
                console.log("delete"); break;
              case 2:  
                this.OpenModalWithData(_id);
                
            }
           
          }
        }
      ]
    })
    alert.present();
  }

  

  parseUrl(url){
    //url="data:image/png;charset=utf-8;base64, "+url;
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
}

