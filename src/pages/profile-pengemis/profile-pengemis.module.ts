import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePengemisPage } from './profile-pengemis';

@NgModule({
  declarations: [
    ProfilePengemisPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePengemisPage),
  ],
})
export class ProfilePengemisPageModule {}
