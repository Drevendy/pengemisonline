import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PengemisDetailPage } from './pengemis-detail';

@NgModule({
  declarations: [
    PengemisDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PengemisDetailPage),
  ],
})
export class PengemisDetailPageModule {}
