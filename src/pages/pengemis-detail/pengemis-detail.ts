import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { DonasiPage } from "../donasi/donasi";
import { PesanPage } from "../pesan/pesan";

/**
 * Generated class for the PengemisDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pengemis-detail',
  templateUrl: 'pengemis-detail.html',
})
export class PengemisDetailPage {
  user : any;
  UserData : any;
  ListOfJasa : any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private domSanitizer : DomSanitizer, private app: App) {
    this.user = this.navParams.get('data_user');  
    this.UserData = this.user.pengemis;
    this.ListOfJasa = this.user.jasa;
  }

  ionViewDidLoad() {
      
    console.log(this.user);
  }
  parseUrl(url){
    //url="data:image/png;charset=utf-8;base64, "+url;
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
  Pesan(jasa){
    console.log(jasa);
  }
  inputDataPemesan(user, jasa){
    this.app.getRootNav().push(PesanPage, {data_user : user, data_jasa : jasa});
  }
  GoToDonasi(){
    this.app.getRootNav().push(DonasiPage, {data_user : this.user});
  }
}
