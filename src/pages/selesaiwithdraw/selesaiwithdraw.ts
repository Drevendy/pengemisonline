import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotifikasiPage } from "../notifikasi/notifikasi";
/**
 * Generated class for the SelesaiwithdrawPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selesaiwithdraw',
  templateUrl: 'selesaiwithdraw.html',
})
export class SelesaiwithdrawPage {
  user : any;
  jumlah : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user = this.navParams.get('user');  
    this.jumlah = this.navParams.get('jumlah_withdraw');  
    console.log(this.user);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelesaiwithdrawPage');
  }
  notif(){
    this.navCtrl.setRoot(NotifikasiPage);
  }

}
