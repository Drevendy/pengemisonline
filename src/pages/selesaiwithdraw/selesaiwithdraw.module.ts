import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelesaiwithdrawPage } from './selesaiwithdraw';

@NgModule({
  declarations: [
    SelesaiwithdrawPage,
  ],
  imports: [
    IonicPageModule.forChild(SelesaiwithdrawPage),
  ],
})
export class SelesaiwithdrawPageModule {}
