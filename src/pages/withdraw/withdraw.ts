import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { AlertController } from "ionic-angular";
import { HomePage } from "../home/home";
import { SelesaiwithdrawPage } from "../selesaiwithdraw/selesaiwithdraw";
import { PengemisOnline }from '../services/PengemisOnline.service';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';

/**
 * Generated class for the WithdrawPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-withdraw',
  templateUrl: 'withdraw.html',
})
export class WithdrawPage {
  a:any;
  loggedInUser : any;
  withdrawForm : FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl:AlertController, private pengemisOnline : PengemisOnline, private app: App) {
    this.loggedInUser = this.pengemisOnline.GetUserData();
    console.log(this.loggedInUser);
  }
  ngOnInit(){
    this.initializeForm();
  }
  private initializeForm(){
    this.withdrawForm = new FormGroup({
      jumlah: new FormControl('', Validators.required),
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad WithdrawPage');
  }



  tarikalert(){
    
     if(this.withdrawForm.value['jumlah']<10000){
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'Jumlah penarikan uang harus lebih dari Rp. 10.000 ,-',
        buttons: [
          {
            text: 'OK',
            handler: () => {
            }
          }
        ]
      });
      alert.present();
     }
     else if(this.withdrawForm.value['jumlah']>this.loggedInUser.saldo){
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'Saldo anda tidak cukup!',
        buttons: [
          {
            text: 'OK',
            handler: () => {
            }
          }
        ]
      });
      alert.present();
     }
     else{
      let alert = this.alertCtrl.create({
        title: 'Tarik Uang',
        message: 'Apakah anda yakin?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'OK',
            handler: () => {
              this.pengemisOnline.Withdraw(this.loggedInUser.id, this.withdrawForm.value['jumlah']).subscribe(data=>{
                console.log(data);
                this.loggedInUser = data;
                console.log(this.loggedInUser);
                this.pengemisOnline.SetUserData(data);
                /*console.log(data[0]['status']);
                if(data[0]['status']){
                  console.log("memek");
                  this.pengemisOnline.setLoginState(data[0]['status']);
                  this.navCtrl.setRoot(HomePage);
                }else{
                  this.pengemisOnline.setLoginState(data[0]['status']);
                  this.loginState=data[0]['status']
                }*/
              });
              this.app.getRootNav().push(SelesaiwithdrawPage, {user : this.loggedInUser, jumlah_withdraw : this.withdrawForm.value['jumlah']});
            }
          }
        ]
      });
      alert.present();
     }
  }
}
