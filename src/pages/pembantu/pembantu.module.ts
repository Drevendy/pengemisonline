import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PembantuPage } from './pembantu';

@NgModule({
  declarations: [
    PembantuPage,
  ],
  imports: [
    IonicPageModule.forChild(PembantuPage),
  ],
})
export class PembantuPageModule {}
