natives:

local notification 
ionic cordova plugin add cordova-plugin-local-notification
npm install --save @ionic-native/local-notifications

sqlite
ionic cordova plugin add cordova-sqlite-storage
npm install --save @ionic-native/sqlite

ionic device
ionic cordova plugin add cordova-plugin-device
npm install --save @ionic-native/device